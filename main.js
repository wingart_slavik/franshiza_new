//----Include----
    if($(".owl-carousel").length){
      include("/asset/js/owl.carousel.js");
    }
    if($(".flexslider").length){
      include("/asset/js/jquery.flexslider.js");
    }
    if($(".data-mh").length){
      include("/asset/js/jquery.matchHeight-min.js");
    }
    if($(".mini_slider").length){
      include("/asset/js/jquery.cycle.all.js");
    }
    if($(".styler").length){
      include("/asset/js/jquery.formstyler.js");
    }
    if($(".sticky_script").length){
      include("/asset/js/jquery.sticky.js");
    }
    if($("#tabs").length){
      include("/asset/js/easyResponsiveTabs.js");
    }
    include("/asset/js/validate_form.js");
    include("/asset/js/jquery.form.min.js");
    include("/asset/js/script2.js");
    include("/asset/js/modernizr.js");
	
//----Include-Function----
    function include(url){
      document.write('<script src="'+ url + '"></script>');
    }
//----Include end----

$(document).ready(function() {
	
	// mini slider on home page
	
  	if($(".mini_slider").length){
  		$(".mini_slider").cycle({ 
  		    fx:    'scrollDown', 
  		    sync:   0,
  		    speed : 1000, 
  		    delay: -2500
  		});
  	}
  // mini slider on home page end

  //---------------------Tabs-----------
  if ($('#tabs').length){
    $( "#tabs" ).easyResponsiveTabs();
  }

  if($(".flexslider").length){
    $('.flexslider').flexslider({
      animation: "slide",
      minItems: 1,
      itemMargin: 10,
      slideshow: false, 
      smoothHeight: true, 
      animationSpeed: 200,
      maxItems: 1
    });
  };

  // formstyler start
  	if($(".styler").length){
  		$(".styler").styler()
  	}
  // formstyler end

  // item(button) start
    if($(".similar_button").length){
      $('div.similar_button').live("click", function () {
        $(this).hide();
        $(this).next().show();
      });  
    }
  // item(button) end
    // filter button click start
      if($(".sort_btn").length){
        $(".sort_btn").on("click", function(){
          $(".sort_btn").toggleClass("active").text("От низких объeмов инвестиций к высоким");
        });
      }
    // filter button click end
    //----owl-carousel-----
      if($('.owl-car1').length){
        $('.owl-car1').each(function(){
          var $this = $(this);
          if($this.hasClass('full-width')){
            $(this).owlCarousel({
              stopOnHover : true,
              rewindNav:false,
              navigation:true,
              paginationSpeed : 2000,
              goToFirstSpeed : 2000,
              singleItem : true,
              autoHeight : true,
              mouseDrag:true
              // transitionStyle:"fade"
            });
          }
          else{
            var itemA = $(this).attr('data-itemA'),
                itemB = $(this).attr('data-itemB'),
                itemC = $(this).attr('data-itemC'),
                itemD = $(this).attr('data-itemD'),
                itemE = $(this).attr('data-itemE');
            $(this).owlCarousel({
                 margin: 10 ,
                 nav: true ,
                 smartSpeed:1000,
                 responsive: {
                    0 : {
                         items: itemE
                    },
                    960 : {
                         items: itemD
                    },
                    1358 : {
                         items: itemC
                    },
                    1600 : {
                         items: itemB
                    },
                    1900 : {
                         items: itemA
                    }
                }
            });
          }
        });
      };
    //----owl-carousel end-----


    // anchor start (index.html)
      
        // $(".top_anchor_btn").on("click", function(){
        //     $(".top_anchor_btn").addClass("active");
        //     $(".top_anchor_btn.active span").text("Перейте к списку (1)");


        //   if($(this).hasClass("first_click")){
        //       var elementClick = $(this).attr("href"),
        //           destination = $(elementClick).offset().top;
        //       $('html,body').animate( { scrollTop: destination }, 1100 );
        //       $(this).removeClass("first_click");
        //       return false;
        //   }
        //   else{
        //       var destination2 = $("#position2").offset().top;
        //       $('html,body').animate( { scrollTop: destination2 }, 1100 );
        //       return false;
        //   }
        // });

        // if($(".top_anchor_btn2").length){
        //   $(".top_anchor_btn2").on("click", function(){
        //     $(".top_anchor_btn2").addClass("active");
        //     $(".top_anchor_btn2.active span").text("Перейте к списку (1)");

        //     if($(this).hasClass("first_clickIt")){
        //         var elementClick1 = $(this).attr("href"),
        //             destinationItem = $(elementClick1).offset().top;
        //         $('html,body').animate( { scrollTop: destinationItem }, 1100 );
        //         $(this).removeClass("first_clickIt");
        //         return false;
        //     }
        //     else{
        //         var destinationItem2 = $("#position22").offset().top;
        //         $('html,body').animate( { scrollTop: destinationItem2 }, 1100 );
        //         return false;
        //     }
        //   });  
        // }
    // anchor end (index.html)

    // всплывающие подсказки на главной странице ===начало====
    
        if($(".quest").length){
          $('body').append('<div id="help-txt"></div>');
            var l=0,t=0,text;$('.quest').live('mouseover',function(){
            if($(this).attr('data-text')){
              text=$(this).attr('data-text');
            }
            else{
              text='<p>Тут должна быть подсказка</p> <span class="icon"></span>';
            }
            $('#help-txt').html('<p>'+text+'</p><span class="icon"></span>');
            l=$(this).offset().left-15-$('#help-txt').width()/2;t=$(this).offset().top-$('#help-txt').innerHeight()+5;
            $('#help-txt').css({left:l,top:t}).animate({opacity:'show'},300);
          });
          $('#help-txt').live('mouseleave',function(){
            $('#help-txt').animate({opacity:'hide'},300);
          });
        }
    // всплывающие подсказки на главной странице ===конец====

    //(все страницы) кнопка вверх при мелиа запросе 1150   ===начало====
        $(".top_anchor").on("click", function(){
              var elementClick = $(this).attr("href"),
                  destination = $(elementClick).offset().top;
              $('html,body').animate( { scrollTop: destination }, 1100 );
              return false;
        });

      if($(".top_anchor").length){
        function upBtn(){
          var windowScroll = $(window).scrollTop(),
              offset = $('.arr_anchor_it').offset().top;
          if(windowScroll>offset){
            $(".top_anchor").fadeIn();
          }else{
            $(".top_anchor").fadeOut();
          }
        }
        upBtn();
        $(window).on('scroll',upBtn);
      }
    //(все страницы) кнопка вверх при медиа запросе 1150   ===конец====

    /*========Выпадашка на главной при 768 =====старт========*/
      if($('.threeblock_one_text').length){
        $('.threeblock_one_text').on('click', function(event){
          $(this)
            .toggleClass('active')
            .next('.down')
            .slideToggle()
            .parents(".fransearch_itembox")
            .siblings(".fransearch_itembox")
            .find(".threeblock_one_text")
            .removeClass("active")
            .next(".down")
            .slideUp();
          event.preventDefault();
        });
      }
    /*========Выпадашка на главной при 768 =====конец========*/

    if ($("#sticker1").length) {
        function sticker(){
          var windowscrollTop = $(window).scrollTop(),
              position1 = $("#sticker1").offset().top,
              position2 = $("#position2").offset().top;
          if($(window).width() <= 768){
            if(position1 < windowscrollTop && windowscrollTop < (position2 - 77)){
              $("#sticker1 > div").addClass("sticker").show();
              $(".btn_fixed").removeClass("top_zero")
            }
            else if(windowscrollTop > (position2 - 77)){
              $("#sticker1 > div").hide();
              $(".btn_fixed").addClass("top_zero")
            }
            else{
              $("#sticker1 > div").removeClass("sticker");
              
            }
          }
        }
        sticker();
        $(window).on("scroll", sticker);
      };
      // sticky
      if($(".stickybtn").length){
        $(".stickybtn").sticky({topSpacing:77});  
      }

      if($("#stickybtn_2").length){
          $("#stickybtn_2").sticky({
            topSpacing:0,
            wrapperClassName:"sticky-wrapper2"
          });
        }


    function scrollTo(target){
      $('html, body').stop().animate({
          scrollTop: $(target).offset().top - 20
      }, 1000);

      console.log(target);
    }

    $(".sticky_btn.scroll_to").on("click", function(event){

      scrollTo($(this).attr("href"));
      event.preventDefault();
    })

    $(".sticky_btn").on("click", ".close_btn", function(){
      $(this).parent().remove();
    })
    // якорь для страницы отзывы старт
      $(".scroll_otzyvy").on("click", function(event){

        scrollTo($(this).attr("href"));
        event.preventDefault();
      })
    // якорь для страницы отзывы финиш
      // удаляет (фильтр на главной в форме) старт
        if($(".delete_btn").length){
         $('.delete_btn').on('click',function(){
            $(this).parents('.basket_fltr_it').remove()
          });
        }
        // if($(".delete_btn2").length){
        //  $('.delete_btn2').on('click',function(){
        //     $(this).parents('.but_d').remove()
        //   });
        // }
      // удаляет (фильтр на главной в форме) финиш


      //  =====================tooltip start=====================
        if($(".toggle_link_extra").length){
           $(".toggle_link_extra, #toggle_box .close").on("click", function(event){
            $("#toggle_box").toggle().addClass("active");
            event.preventDefault();
          })

          $(document).on("click touchstart", function(event) {
            if ($(event.target).closest("#toggle_box,.toggle_link_extra").length) return;
            $("#toggle_box.active").hide();
            event.stopPropagation();
          })
        }
      //  =====================tooltip end=======================


})

$(window).load(function(){
  //скрипт для того чтобы элементы правильно перестраивались на тач устройствах и на компе (он считывает сколько ширина скрола)
  var scrollWidth;
  function detectScrollBarWidth(){
      var div = document.createElement('div');
      div.className = "detect_scroll_width";
      document.body.appendChild(div);
      scrollWidth = div.offsetWidth - div.clientWidth;
      document.body.removeChild(div);
      console.log(scrollWidth)
  }
    detectScrollBarWidth();
  //скрипт для того чтобы элементы правильно перестраивались на тач устройствах и на компе (он считывает сколько ширина скрола) 
  // меняем позицию блоков
        // ===========blockPosition start главная===============
        function blockPosition(){
          var bodyWidth = $(window).width();
          if(bodyWidth + scrollWidth <= 1199 && $('body').hasClass('body_r')){
            $('.left_col').prepend($('.search_box'));
            $('.right_col').append($('.left_news_block'));
            $('body').removeClass('body_r');
          }
          else if(bodyWidth + scrollWidth > 1199 && !$('body').hasClass('body_r')){
            $('.right_col').prepend($('.search_box'));
            $(".left_slides_block2").after($('.left_news_block'));
            $('body').addClass('body_r');
          }

          
         }
         blockPosition();
        // ===========blockPosition end===============

        // ===========blockPosition1 start ===============
        function blockPosition1(){
          var bodyWidth = $(window).width();
          if(bodyWidth + scrollWidth <= 1023 && $('body').hasClass('body_r1')){
            $('.right_col').append($('.left_subscribe_block'));
            $('.right_col').append($('.left_slides_block2'));
            $('body').removeClass('body_r1');
          }
          else if(bodyWidth + scrollWidth > 1023 && !$('body').hasClass('body_r1')){
            $(".search_box").after($('.left_slides_block2'));
            $(".left_col").append($('.left_subscribe_block'));
            $('body').addClass('body_r1');
          }
          // console.log(2);
        }
        blockPosition1();
        // ===========blockPosition1 end===============

        // ===========blockPosition2 start новости===============
        function blockPosition2(){
          var bodyWidth = $(window).width();
          if(bodyWidth + scrollWidth <= 479 && $('body').hasClass('body_r2') && !$('div').hasClass('not_news_page')){
            $('.doplinks_after_scr').after($('.step_main_block'));
            $('body').removeClass('body_r2');
          }
          else if(bodyWidth + scrollWidth > 479 && !$('body').hasClass('body_r2')){
            $(".right_col").prepend($('.step_main_block'));
            $('body').addClass('body_r2');
          }
        }
        blockPosition2();
        // ===========blockPosition2 end===============

        // ===========blockPosition3 start елки-палки===============
        function blockPosition3(){
          var bodyWidth = $(window).width();
          if(bodyWidth + scrollWidth <= 1023 && $('body').hasClass('body_r3')){
            $('.franshiza_single_box').prepend($('.franshiza_single_box_caption'));
            $('body').removeClass('body_r3');
          }
          else if(bodyWidth + scrollWidth > 1023 && !$('body').hasClass('body_r3')){
            $(".franshiza_single_box_img").after($('.franshiza_single_box_caption'));
            $('body').addClass('body_r3');
          }
        }
        blockPosition3();
        // ===========blockPosition3 end===============
        // ===========blockPosition4 start страница отзывы===============
        function blockPosition4(){
          var bodyWidth = $(window).width();
          if(bodyWidth + scrollWidth <= 1023 && $('body').hasClass('body_r4')){
            $('.right_col').prepend($('.otzyvy_left_main'));
            $('body').removeClass('body_r4');
          }
          else if(bodyWidth + scrollWidth > 1023 && !$('body').hasClass('body_r4')){
            $('.left_col').prepend($('.otzyvy_left_main'));
            $('body').addClass('body_r4');
          }
        } 
        blockPosition4();
        // ===========blockPosition4 end===============
        $(window).on('resize',function(){
          setTimeout(function(){
            blockPosition1();
            setTimeout(function(){
              blockPosition();
            },100);
            blockPosition2();
            blockPosition3();
            blockPosition4();
          }, 500);
        });
});