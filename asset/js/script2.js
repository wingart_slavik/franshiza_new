

$(function(){
	$('div.similar_button[data-id]').live("click", function () {
		console.log($(this).attr('data-id'));
		$('div.block_form_item').hide();
		$('#basket').show();
		var id = $(this).attr('data-id');
		var name = $(this).attr('data-name');
		var url = $(this).attr('data-url');
		var img = $(this).attr('data-img');
		var A = {
			action : 'addToBasket',
			id     : id
		};
		$.post('/ajax/ajax.php', A , function(data){
			var block = ''+
		            	'<div class="f_left basket_fltr_it data-mh" data-mh="basket_fltr_it" data-id="'+id+'">'+
		                    '<div class="basket_fltr_img">'+
		                        '<a href="'+url+'"><img src="'+img+'" style="max-width:54px;"></a>'+
		                    '</div>'+
		                    '<div class="basket_fltr_lnk">'+
		                        '<a href="'+url+'">'+name+'</a>'+
		                        '<div class="del delete_btn" data-delete="'+id+'">Убрать</div>'+
		                    '</div>'+
		                '</div>';

			$('div#basket .list').append(block);
			$('div.full-sell-link[data-id="'+id+'"]').hide();
			$('div.gotosell[data-id="'+id+'"]').show();
			$('#stickybtn_2 span').text('Ваш список ('+data+')');
		})
	});
	
	$('div[data-delete]').live("click" , function(){
		var id = $(this).attr('data-delete');
		console.log($(this));
		deleteFromBasket(id);
	})

    $('#filterform div.form_row input,#filterform label.form_label div.jq-checkbox').click(function(){
        FilterFranshiz();
    });

    $('#filterform2 div.form_row input,#filterform2 label.form_label div.jq-checkbox').click(function(){
        FilterFranshiz2();
    });
    
    $('#sort_elm').click(function(){
	    if($(this).hasClass('active')){
		    $('#sort_field').val('asc');
	    }else{
		    $('#sort_field').val('desc');
	    }
	    FilterFranshiz();
    })
	
	$('a#add2Baskets').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		var added = $(this).attr('data-added');
		if(added == 0)
		{
			var A = {
				action : 'addToBasket',
				id     : id
			};
			$.post('/ajax/ajax.php', A , function(){
				$('a#add2Baskets').attr('data-added','1');
				$('a#add2Baskets').css('background-color','red');
				$('a#add2Baskets').text('Перейти к списку');
			});
		}
		else
		{
			document.location.href = '/#st3';
		}	
	})    
    
})

function deleteFromBasket(id)
{
	var A = {
		action : 'addToBasket',
		id     : id
	};
	$.post('/ajax/ajax.php', A , function(data){
		$('#basket div[data-mh][data-id="'+id+'"]').remove();
		$('div.similar_button[data-id="'+id+'"]').show();
		$('div.similar_button2[data-id="'+id+'"]').hide();	
		if(!$('#basket div[data-mh]').length)
		{
			$('#stickybtn_2 span').text('Ваш список пуст');
			$('div.block_form_item').show();
			$('#basket').hide();
		}
		else
		{
			$('#stickybtn_2 span').text('Ваш список ('+$('#basket div[data-mh]').length+')');
		}	
	});			
}

function FilterFranshiz() {
    //alert(1);
    var options = {
        url: "/ajax/FilterAction.php",
        success: function(dat) {
            $('#main_list_franch').replaceWith(dat);
            start_carousel();
        }
    }
    if ($("#filterform").length > 0)
        $("#filterform").ajaxSubmit(options);
}

function FilterFranshiz2() {
    //alert(1);
    var options = {
        url: "/ajax/FilterAction.php",
        success: function(dat) {
            $('#main_list_franch').replaceWith(dat);
            start_carousel();
        }
    }
    if ($("#filterform2").length > 0)
        $("#filterform2").ajaxSubmit(options);
}

function FilterFranshizPrice() {
    var options = {
        url: "/ajax/FilterAction.php",
        success: function(dat) {
            $('#list_box').html(dat);
            start_carousel();
        }
    }
    console.dir(options);
    if ($("#filterform").length > 0)
        $("#filterform").ajaxSubmit(options);
}

function ClearPrice() {
    $("#flag").val(0);
    var min = $('.invest .min .none').html(),
        max = $('.invest .max .none').html();
    $("#left_input").val(min);
    $("#right_input").val(max);
    $(".range").slider("option", "values", [min, max]);
    str = min.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    $(".minval").html(str);
    str = max.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    $(".maxval").html(str);
    $("#flag").val(1);
    FilterFranshizPrice();
}

function ClearFilter() {
    var min = $('.invest .min .none').html(),
        max = $('.invest .max .none').html();
    $("#right_input").val('');
    $('#pr_max').attr('checked', 'checked');
    $("#flag").val(0);
    $("#flag").val(1);
    $(".fransearch input[type=checkbox]").each(function() {
        if ($(this).val() > 0) {
            var elclass = $(this).parent().next().attr("class");
            if (elclass.indexOf("grey") > 1) {
                $(this).parent().next().removeClass("grey");
                $(this).parent().removeClass("notactive");
            }
            var inp = $(this).parent('.fcheck');
            if (inp.hasClass('active')) {
                inp.removeClass('active').find('input').removeAttr('checked');
            }
        }
    });
    FilterFranshiz();
}

function CheckBoxForTags(id) {
    if (id == 'max_price') {
        $("#right_input").val('');
        $('#pr_max').attr('checked', 'checked');
    } else {
        var inp = $("#chk" + id).parent();
        if (!inp.hasClass('active')) {
            inp.addClass('active').find('input').attr('checked', 'checked');
        } else {
            inp.removeClass('active').find('input').removeAttr('checked');
        }
    }
    FilterFranshiz();
}

function start_carousel()
{
	if($('.owl-car1').length){
    $('.owl-car1').each(function(){
      var $this = $(this);
      if($this.hasClass('full-width')){
        // $(this).owlCarousel({
        //     margin: 10 ,
        //     nav: true ,
        //     mouseDrag:true,
        //     autoHeight : true,
        //     items:1
        // });
        $(this).owlCarousel({
          stopOnHover : true,
          rewindNav:false,
          navigation:true,
          paginationSpeed : 2000,
          goToFirstSpeed : 2000,
          singleItem : true,
          autoHeight : true,
          mouseDrag:true
          // transitionStyle:"fade"
        });
      }
      else{
        var itemA = $(this).attr('data-itemA'),
            itemB = $(this).attr('data-itemB'),
            itemC = $(this).attr('data-itemC'),
            itemD = $(this).attr('data-itemD'),
            itemE = $(this).attr('data-itemE');
        $(this).owlCarousel({
             margin: 10 ,
             nav: true ,
             smartSpeed:1000,
             responsive: {
                0 : {
                     items: itemE
                },
                960 : {
                     items: itemD
                },
                1358 : {
                     items: itemC
                },
                1600 : {
                     items: itemB
                },
                1900 : {
                     items: itemA
                }
            }
        });
      }
    });
  };
}

function refreshParams(){
	$('div.fcheck').addClass('notactive');
					
}
